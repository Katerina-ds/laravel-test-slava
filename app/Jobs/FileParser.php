<?php

namespace App\Jobs;

use App\Models\Row;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Spatie\SimpleExcel\SimpleExcelReader;

class FileParser implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    const BATCH_SIZE = 1000;
    /**
     * Create a new job instance.
     */

    public function __construct(private readonly string $filePath)
    {
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        try {
            $reader = SimpleExcelReader::create($this->filePath);
        } catch (Exception $e) {
            Log::error('Parsing error');
        }

        $processedRowsTotal = 0;
        $countRows = 0;

        do {
            $rows =
                $reader
                    ->skip($processedRowsTotal)
                    ->take(self::BATCH_SIZE)
                    ->getRows()
                    ->map(function (array $rowProperties) {
                        return [
                            'name' => $rowProperties['name'],
                            'date' => $rowProperties['date']
                        ];
                    })
                    ->toArray();

            Row::insert($rows);

            $countRows = count($rows);

            $processedRowsTotal += $countRows;

            Redis::set('parsing-progress', $processedRowsTotal);
        } while ($countRows >= self::BATCH_SIZE);
    }
}
