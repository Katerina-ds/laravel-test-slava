<?php

namespace App\Actions;

use App\Models\Row;

class RowsAction
{
    public function getRows(int $limit, int $offset)
    {
        return
            Row::orderBy('date')
                ->limit($limit)
                ->offset($offset)
                ->get()
                ->groupBy('date');
    }
}
