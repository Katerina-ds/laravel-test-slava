<?php

namespace App\Actions;

use App\Jobs\FileParser;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ExcelAction
{
    public function execute(UploadedFile $file)
    {
        $filePath = $file->store('uploads');

        FileParser::dispatch(Storage::path($filePath));

        Storage::delete($filePath);
    }
}
