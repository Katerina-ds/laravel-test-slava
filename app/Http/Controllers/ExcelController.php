<?php

namespace App\Http\Controllers;

use App\Actions\ExcelAction;
use App\Actions\RowsAction;
use App\Http\Requests\ExcelRequest;
use App\Http\Requests\RowsRequest;

class ExcelController
{
    public function import(ExcelRequest $request, ExcelAction $action)
    {
        $file = $request->validated()['file'];

        $action->execute($file);

        return response()->json(['message' => 'The file was added to parsing queue']);
    }

    public function index(RowsRequest $request, RowsAction $action)
    {
        $params = $request->validated();

        return $action->getRows($params['limit'], $params['offset']);
    }
}
