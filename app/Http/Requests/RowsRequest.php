<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RowsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'limit' => 'required|int|max:30|min:0',
            'offset' => 'required|int|min:0'
        ];
    }
}
