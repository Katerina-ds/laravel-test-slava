<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $exception)
    {
        if ($exception instanceof UnauthorizedHttpException) {
            return response()->json(
                [
                    'message' => $exception->getMessage()
                ],
                $exception->getStatusCode()
            );
        }

        if ($exception instanceof ValidationException) {
            $errors = [];
            foreach ($exception->errors() as $key => $error) {
                $errors[$key] = $error[0];
            }

            return response()->json(
                [
                    'message' => $exception->getMessage(),
                    'errors' => $errors
                ],
                $exception->status
            );
        }

        return parent::render($request, $exception);
    }
}
