<?php

namespace Tests\Unit\Jobs;

use App\Jobs\FileParser;
use App\Models\Row;
use Carbon\Carbon;
use Spatie\SimpleExcel\SimpleExcelReader;
use Tests\TestCase;

class FileParserTest extends TestCase
{
    public string $path = 'tests/Unit/Jobs/test_small.xlsx';

    public function testShouldReadXlsxCorrectly()
    {
        $reader = SimpleExcelReader::create($this->path);

        $rows =
            $reader
                ->skip(0)
                ->take(5)
                ->getRows()
                ->map(function (array $rowProperties) {
                    return [
                        'name' => $rowProperties['name'],
                        'date' => $rowProperties['date']
                    ];
                })
                ->toArray();

        $expected = [
            ['name' => 'Denim', 'date' => Carbon::parse('2020-10-13 00:00:00')],
            ['name' => 'Denim', 'date' => Carbon::parse('2020-10-14 00:00:00')],
            ['name' => 'Denim', 'date' => Carbon::parse('2020-10-15 00:00:00')],
            ['name' => 'Denim', 'date' => Carbon::parse('2020-10-16 00:00:00')],
            ['name' => 'Denim', 'date' => Carbon::parse('2020-10-17 00:00:00')],
        ];

        $this->assertEquals($rows, $expected);
    }

    public function testShouldReadXlsxAndFillRows()
    {
        FileParser::dispatch($this->path);

        $this->assertEquals(2474, Row::count());
    }
}
