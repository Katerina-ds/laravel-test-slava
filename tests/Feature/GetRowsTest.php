<?php

namespace Tests\Feature;

use App\Models\Row;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetRowsTest extends TestCase
{
    private string $endpoint = 'api/rows';

    public function testShouldReturnValidationErrorsIfNoParametersGiven()
    {
        $response = $this->call('GET', $this->endpoint);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testShouldReturnValidationErrorsIfBadParametersGiven()
    {
        $response = $this->call('GET', $this->endpoint, ['limit' => 300, 'offset' => 'test']);

        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson(
                [
                    'message' => 'The limit field must not be greater than 30. (and 1 more error)',
                    'errors' => [
                        'limit' => 'The limit field must not be greater than 30.',
                        'offset' => 'The offset field must be an integer.'
                    ]
                ]
            );
    }

    public function testShouldReturnOk()
    {
        $this->prepareEnvironment();

        $response = $this->call('GET', $this->endpoint, ['limit' => 4, 'offset' => 0]);
        $response->assertStatus(Response::HTTP_OK);

        $responseNext = $this->call('GET', $this->endpoint, ['limit' => 4, 'offset' => 1]);
        $responseNext->assertStatus(Response::HTTP_OK);

        $this->assertNotEquals($response->getContent(), $responseNext->getContent());

        $responseLast = $this->call('GET', $this->endpoint, ['limit' => 30, 'offset' => 3]);
        $responseLast->assertStatus(Response::HTTP_OK)->assertJson([]);
    }

    private function prepareEnvironment()
    {
        Row::factory()->count(50)->create();
    }
}
