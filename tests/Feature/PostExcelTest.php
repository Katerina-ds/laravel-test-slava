<?php

namespace Feature;

use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class PostExcelTest extends TestCase
{
    private string $endpoint = 'api/import-from-excel';

    public function testShouldReturnUnauthorized()
    {
        $response = $this->call('POST', $this->endpoint);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testShouldReturnValidationErrorsIfNoParametersGiven()
    {
        $response = $this->call('POST', $this->endpoint, server: $this->authorize());

        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonFragment(
                [
                    'message' => 'The file field is required.',
                ]
            );
    }

    public function testShouldReturnValidationErrorsIfBadParametersGiven()
    {
        $response = $this->call('POST', $this->endpoint, ['file' => 300], server: $this->authorize());

        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson(
                [
                    'message' => 'The file field must be a file. (and 1 more error)',
                    'errors' => [
                        'file' => 'The file field must be a file.',
                    ]
                ]
            );
    }

    public function testShouldReturnValidationErrorIfIncorrectFileGiven()
    {
        $file = UploadedFile::fake()->create(
            'test.json',
            6,
            'application/json'
        );

        $response = $this->call('POST', $this->endpoint, ['file' => $file], server: $this->authorize());

        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson(
                [
                    'errors' =>
                        [
                            'file' => 'The file field must be a file of type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.'
                        ],
                    'message' => 'The file field must be a file of type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.'
                ]
            );
    }

    private function authorize(): array
    {
        $email = config('app.admin.email');
        $password = config('app.admin.password');
        User::factory()->create(['email' => $email, 'password' => $password]);

        return ['PHP_AUTH_USER' => $email, 'PHP_AUTH_PW' => $password];
    }
}
