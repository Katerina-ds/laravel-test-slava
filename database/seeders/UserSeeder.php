<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate(
            ['email' => config('app.admin.email')],
            [
                'email' => config('app.admin.email'),
                'name' => 'SuperAdmin',
                'password' => config('app.admin.password'),
            ]
        );
    }
}
